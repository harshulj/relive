from django.conf.urls import patterns, include, url
from rest_framework import routers
from main.views import index, AppUserView
from lifeevent.views import LifeEventViewSet

from django.contrib import admin
admin.autodiscover()

router = routers.DefaultRouter()
router.register(r'events', LifeEventViewSet, base_name='.')

urlpatterns = patterns('',
    url(r'^$', index),
    url(r'^me$', AppUserView.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += router.urls

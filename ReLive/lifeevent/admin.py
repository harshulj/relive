from django.contrib import admin
from lifeevent.models import LifeEvent

#class LifeEventImageInline(admin.TabularInline):
#    model = LifeEventImage
#    extra = 3

class LifeEventAdmin(admin.ModelAdmin):
    pass
#    inlines = [LifeEventImageInline, ]

admin.site.register(LifeEvent, LifeEventAdmin)

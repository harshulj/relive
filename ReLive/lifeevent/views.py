from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from lifeevent.models import LifeEvent
from lifeevent.serializers import LifeEventSerializer
from main.permissions import IsOwnerOrReadOnly

class LifeEventViewSet(viewsets.ModelViewSet):
    serializer_class = LifeEventSerializer
    permission_classes = (IsOwnerOrReadOnly, IsAuthenticated,)
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def get_queryset(self):
        return self.request.user.life_events.all()

    def pre_save(self, obj):
        obj.user = self.request.user


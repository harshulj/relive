from lifeevent.models import LifeEvent#, LifeEventImage
from rest_framework import serializers

#class LifeEventImageSerializer(serializers.ModelSerializer):
#    class Meta:
#        model = LifeEventImage
#        fields = ('id', 'image')

class LifeEventSerializer(serializers.ModelSerializer):
#    image = LifeEventImageSerializer(many=True)

    class Meta:
        model = LifeEvent
        fields = ('id','dTime', 'place', 'description', 'title', 'user', 'image')
        read_only_fields = ('user',)


from django.db import models
from ReLive import settings

class LifeEvent(models.Model):
    dTime = models.DateTimeField(verbose_name="Date Time Event Happened")
    place = models.CharField(max_length=50, verbose_name="Place Event Occured")
    description = models.CharField(max_length=150, verbose_name="Describe the Event")
    title = models.CharField(max_length=50, verbose_name="Event Title")
    created_at = models.DateTimeField(verbose_name="Date Time Event Created", auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name="Date Time Event last updated", auto_now=True)
    image = models.ImageField(null=True, upload_to='media/lifeeventimages/')

    user = models.ForeignKey('main.AppUser', related_name='life_events')


    def __str__(self):
        return "Life Event: %s at %s on %s"%(self.title, self.place, self.dTime)

    class Meta:
        ordering = ['dTime']

#class LifeEventImage(models.Model):
#    event = models.ForeignKey(LifeEvent, related_name="images")
#    image = models.ImageField(upload_to=settings.MEDIA_DIR + 'lifeeventimages/')


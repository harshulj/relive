from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.parsers import JSONParser
from main.permissions import IsOwnerOrReadOnly
from main.serializers import AppUserSerializer

def index(request):
    c= {}
    c.update(csrf(request))
    c['user'] = request.user
    if request.is_mobile:
        return render_to_response('mobile/index.html', c)
    else:
        return render_to_response('web/index.html', c)

class AppUserView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly,)

    def get_object(self, username):
        try:
            return AppUser.objects.get(username=username)
        except AppUser.DoesNotExist:
            return Http404

    def post(self, request, format=None):
        import ast
        data = ast.literal_eval(request.POST['_content'])
        print isinstance(data, dict)
        appUser = AppUserSerializer(request.user, data=data, partial=True)
        print appUser.__dict__
        if appUser.is_valid():
            appUser.save()
            return Response(appUser.data, status=201)
        return Response(appUser.errors, status=400)


    def get(self, request):
        serializer = AppUserSerializer(request.user, context={'request': request})
        return Response(serializer.data)


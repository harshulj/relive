from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from main.models import AppUser
from main.forms import AppUserCreationForm


class AppUserAdmin(UserAdmin):
    fieldsets = (
            (None, {'fields': ('email', 'username', 'password')}),
            (_('Personal info'), {'fields': ('first_name', 'last_name')}),
            (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                'groups', 'user_permissions')}),
            (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
            )
    add_fieldsets = (
            (None, {
                'classes': ('wide',),
                'fields': ('email', 'username','password1', 'password2')}
                ),
            )
    add_form = AppUserCreationForm
    list_display = ('email','username', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'username', 'first_name', 'last_name')
    ordering = ('username',)

admin.site.register(AppUser, AppUserAdmin)

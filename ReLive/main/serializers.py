from main.models import AppUser
from rest_framework import serializers
from lifeevent.serializers import LifeEventSerializer

class AppUserSerializer(serializers.HyperlinkedModelSerializer):
    life_events = LifeEventSerializer(many=True)

    class Meta:
        model = AppUser
        fields = ('username', 'first_name', 'last_name', 'life_events')
        read_only_fields = ('username', 'email', 'password',)

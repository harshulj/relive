from django.forms import ValidationError
from django.contrib.auth.forms import UserCreationForm
from main.models import AppUser

class AppUserCreationForm(UserCreationForm):
    def __init__(self, *args, **kargs):
        super(AppUserCreationForm, self).__init__(*args, **kargs)
        self.fields['email'].required = True

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            self._meta.model._default_manager.get(username=username)
        except self._meta.model.DoesNotExist:
            return username
        raise ValidationError(self.error_messages['duplicate_username'])

    class Meta:
        model = AppUser


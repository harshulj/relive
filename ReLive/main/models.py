from django.db import models
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser, PermissionsMixin, BaseUserManager


class AppUserManager(BaseUserManager):
    def _create_user(self, email, username, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError("Email address is necessary")
        if not username:
            raise ValueError("Username is necessary")
        email = self.normalize_email(email)
        user = self.model(email=email, username=username, is_staff=is_staff, is_superuser=is_superuser, last_login=now, date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, username, password=None, **extra_fields):
        return self._create_user(email, username, password, False, False, **extra_fields)

    def create_superuser(self, email, username, password=None, **extra_fields):
        return self._create_user(email, username, password, True, True, **extra_fields)


class AppUser(AbstractUser):
    #username = models.CharField(max_length=50, unique=True, db_index=True)
    #email = models.EmailField(max_length=50, unique=True, db_index=True)
    #first_name = models.CharField(max_length=30, default="first Name")
    #last_name = models.CharField(max_length=30, default="Last Name")

    #is_staff = models.BooleanField(default=False)
    #is_active = models.BooleanField(default=True)
    #date_joined = models.DateTimeField(default=timezone.now())
    objects = AppUserManager()


    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return "users/%s/" % urlquote(self.username)

    def get_full_name(self):
        return '%s %s' %(self.first_name, slef.last_name)

    def get_short_name(self):
        return self.first_name


from fabric.api import *
from fabric.colors import green, red


def server() :
    """This pushes to the EC2 instance defined below"""
    # The Elastic IP to your server
    env.host_string = '54.254.244.246'
    # your user on that system
    env.user = 'ubuntu'
    # Assumes that your *.pem key is in the same directory as your fabfile.py
    env.key_filename = '~/.ssh/Singapore-1.pem'



def staging() :
    # path to the directory on the server where your vhost is set up
    path = "/home/ubuntu/apps/relive/"
    appPath = path + "ReLive/"
    # name of the application process
    process = "staging"

    with cd("%s" % path) :
        run("pwd")
        print(red("Beginning Deploy:"))
        print(green("Pulling master from GitHub..."))
        run("git pull origin master")

        print(green("Installing requirements..."))
        run("source %s/env/bin/activate && pip install -r packages.txt" % path)

        print(green("Collecting static files..."))
        run("source %s/env/bin/activate && python %smanage.py collectstatic --noinput" % (path, appPath))

        print(green("Syncing the database..."))
        run("source %s/env/bin/activate && python %smanage.py syncdb" % (path, appPath))

        #print(green("Migrating the database..."))
        #run("source %s/env/bin/activate && python %smanage.py migrate" % (path, appPath))

        #print(green("Restart the uwsgi process"))
        #run("sudo service %s restart" % process)
        print(red("DONE!"))

